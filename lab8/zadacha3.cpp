#include <iostream>
using namespace std;

bool is_nice(int n)
{
	while (n) // while n is not 0
	{
		int d = n % 10;
		if (d % 2 != 0)
			return false;
		n /= 10;
	}
	return true;
}

int main()
{
	int n;
	while (cin >> n)
	{
		if (n < 2)
		{
			cout << "N/A" << endl;
		}
		else if (is_nice(n))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}

	return 0;
