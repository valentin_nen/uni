#include <iostream>
using namespace std;

bool is_prime(int n)
{

	for (int i = 2; i < n; i++)
	{
		if (n%i == 0)
		{
			return false;
		}
	}
	return true;
}

int main()
{
	int n;
	while (cin >> n)
	{
		if (n < 2)
		{
			cout << "N/A" << endl;
		}
		else if (is_prime(n))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}

	return 0;
