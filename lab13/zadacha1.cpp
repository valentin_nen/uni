#include <iostream>
#include <vector>

using namespace std;

void prn(const vector<int> &v)
{
    for(int i=0; i<v.size(); i++)
        cout << v[i] << " ";
    cout << endl;
}

int main()
{
    int n;
    cin >> n;
    vector <int> a(n);
    for (int i=0; i<n; i++)
        cin >> a[i];

    vector <int> v;
    int el;
    while(cin >> el)
        v.push_back(el);

    cout << v[4];
    v.insert(v.begin()+7, 90);
    v.erase(v.begin()+7);

		return 0;
}
