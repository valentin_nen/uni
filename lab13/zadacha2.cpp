#include <iostream>
#include <vector>

using namespace std;

void prn(const vector<int> &v)
{
    for(int i=0; i<v.size(); i++)
        cout << v[i] << " ";
    cout << endl;
}

double exec_avg(vector<double> &v)
{  if (v.size() == 0) return 0;
   int i;
   double sum = 0;
   for (i = 0; i < v.size(); i++)  sum = sum + v[i];
   return sum / v.size();
}

void exec_ins(vector<double> &v)
{
    double value;
    cin >> value;
    v.push_back(value);
}

void exec_del(vector(double> &v))
{
    int pos;
    cin >> pos;
 //tbc..

}

int main()
{
    int n;
    cin >> n;
    vector <double> a(n);
    for (int i=0; i<n; i++)
        cin >> a[i];

    string cmd;
    while(cin >> cmd)
    {
        if(cmd == "sum")
            exec_sum(a);
        else if(cmd == "avg")
            exec_avg(a);
        else if(cmd == "min")
            exec_min(a);
        else if(cmd == "max")
            exec_max(a);
        else if(cmd == "ins")
            exec_ins(a);
        else if(cmd == "del")
            exec_del(a);
    }
        return 0;
}
