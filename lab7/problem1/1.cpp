/***
FN:F00000
PID:1
GID:1
*/

#include <iostream>
using namespace std;

//funcions here


double add(double a, double b)
{
    return a+b;
}

double substract(double a, double b)
{
    return a-b;
}
double multiply(double a, double b)
{
    return a*b;
}
double divide(double a, double b)
{
    return a/b;
}

double do_the_math(double a, double b, char op)
{
    if('+' == op)
        return add(a, b);
    else if ('-' == op)
        return substract (a, b);
    else if ('*' == op)
        return multiply(a, b);
    else if ('/' == op)
        return divide(a, b);
}

int main()
{
	double a, b;
	char op;
	//read
	cin >> a >> b >> op;

	//compute
	double result = do_the_math(a, b, op);

	//output
	cout << result << endl;

	return 0;
}
