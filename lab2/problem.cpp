#include <iostream>
#include <string>

using namespace std;

int main()
{
 string firstName,lastName,fullname;
 cout << "Enter your first name: ";
 getline (cin,firstName);
 cout << "Enter your last name: ";
 getline (cin,lastName);
 fullname = firstName + " " + lastName;
 cout << "Hello, " << fullname << endl;

 return 0;
}

