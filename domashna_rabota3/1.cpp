/***
FN:F79045
PID:1
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

double proizv(vector<double> a, vector<double> b)
{
    double x = a[0] * b[0];
    for (int i = 1 ; i < a.size() ; i ++)
    {
        x-= a[i]*b[i];
    }
    return x;
}

int main()
{
    int n;
    cin >> n;
    vector<double> a(n);
    vector<double> b(n);
    for (int i = 0; i < n; i++)
    cin >> a[i];
    for (int i = 0; i < n; i++)
    cin >> b[i];
    cout << proizv(a,b) << endl;
    return 0;
}
