/***
FN:F79045
PID:3
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void add(vector<double>& a)
{
    double poz, chislo;
    cin >> poz;
    cin >> chislo;
    poz-=1;
    a.resize(a.size()+1);
    for(int i=a.size(); i>poz; i--)
        a[i]=a[i-1];
        a[poz]=chislo;
}

void remove_elem(vector<double>& a)
{
    int poz;
    cin >> poz;
    poz-=1;
    for(int i = poz; i < a.size(); i++)
        a[i]=a[i+1];
    a.resize(a.size()-1);
}

void reverse_vec(vector<double>& a)
{
    int j=a.size()-1;
    vector<double> b(a.size());
    for (int i=0; i<a.size(); i++)
        {
        b[i]=a[j];
        j--;
        }
    a=b;
}

void shift_l(vector<double>& a)
{
    double temp = a[0];
    for (int i = 0; i < a.size(); i++)
    a[i]=a[i+1];
    a[a.size()-1]=temp;
}

void print(vector<double> a)
{
    for(int i=0 ; i < a.size() ; i++)
    cout << a[i] << " ";
    cout << endl;
}

int main()
{
    int n;
    cin >> n;
    vector<double> a(n);
    for (int i = 0; i < n; i++)
        cin >> a[i];

    string cmd;
    while(cin >> cmd, cmd!="exit")
    {
     if (cmd == "add")
        add(a);
     else if (cmd == "remove")
        remove_elem(a);
     else if (cmd == "reverse")
        reverse_vec(a);
     else if (cmd == "shift_left")
        shift_l(a);
     else if (cmd == "print")
        print(a);
    }

	return 0;
}
