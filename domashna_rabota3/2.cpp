/***
FN:F79045
PID:2
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;


void shift_r(vector<double>& a)
{
    double temp = a[a.size()-1];
    for(int i = a.size()-1 ; i > 0 ; i--)
    {
        a[i] = a[i-1];
    }
        a[0] = temp;
}

int main()
{
    vector<double> a;
    double x;
    while(cin >> x)
    {
        a.push_back(x);
    }
    shift_r(a);
    for(int i = 0 ; i < a.size(); i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}
