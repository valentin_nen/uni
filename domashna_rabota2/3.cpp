/***
FN:F79045
PID:1
GID:2
*/

#include <iostream>

using namespace std;

/* Bounds for grades */
double gradeF = 2.5;
double gradeE = 3;
double gradeD = 3.5;
double gradeC = 4.5;
double gradeB = 5.5;
double gradeA = 6;

/* Returns the letter corresponding to the number 2-6 */
char calculateGrade(double number)
{
    if (number < gradeF)
        return 'F';
    else if (number >= gradeF && number < gradeE)
        return 'E';
    else if (number >= gradeE && number < gradeD)
        return 'D';
    else if (number >= gradeD && number < gradeC)
        return 'C';
    else if (number >= gradeC && number < gradeB)
        return 'B';
    else if (number >= gradeB && number < gradeA) //could just write else but not the best practice
        return 'A';
    return ' ';
}

int main()
{
    char facultyNumber[6];
    double grade;

    cin >> facultyNumber;
    cin >> grade;

    if (grade < 2 || grade > 6)
    {
        cout << "Invalid grade" << endl;
        return 0;
    }

    cout << facultyNumber << "\t" << calculateGrade(grade) << endl;

    return 0;
}
