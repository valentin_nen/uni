/***
FN:F79045
PID:1
GID:2
*/

#include <iostream>

using namespace std;

/* Finds the sum of the digits in a number */
int sumDigits(int number)
{
    int sum = 0;

    while (number > 0)
    {
        sum += number % 10;
        number /= 10;
    }

    return sum;
}

int main()
{
    int number;
    cin >> number;

    cout << "Sum of digits: " << sumDigits(number) << endl;

    return 0;
}
