/***
FN:F79045
PID:1
GID:2
*/

#include <iostream>

using namespace std;

/* Finds the min of the numbers in the array */
int findMin(int numbers[], int length)
{
    int min = numbers[0];

    for (int i = 1; i < length; i++)
    {
        if (min > numbers[i])
        {
            min = numbers[i];
        }
    }

    return min;
}

int main()
{
    int totalNumbers;
    cin >> totalNumbers;

    int numbers[totalNumbers];

    for (int i = 0; i < totalNumbers; i++)
    {
        cin >> numbers[i];
    }

    cout << "Min number: " << findMin(numbers, totalNumbers) << endl;

    return 0;
}
