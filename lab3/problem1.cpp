#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    cout << "Enter a number: ";
    int n;
    int p;
    cin >> n;
    p = n*n;
    cout << n << "^2=" << p << ", ";
    p *= n;
    cout << n << "^3=" << p << ", ";
    p *= n;
    cout << n << "^4=" << p << ", ";
    p *= n;
    cout << n << "^5=" << p << ", ";
    p *= n;

    cout << endl;
    return 0;
}
