#include <iostream>
using namespace std;

int main()
{
    double a, b, largest;
    cout << "Enter number 1: ";
    cin >> a;
    cout << "Enter number 2: ";
    cin >> b;
    if (a > b)
        largest = a;
    else
        largest = b;

    //largest = a > b ? a : b;//

    cout << "The largest number is: " << largest << endl;

    return 0;
}
