#include <iostream>

using namespace std;

int main()
{
    string first, middle, last, gender;
    cin >> first >> middle >> last >> gender;

    string initials;

    initials.append(first.substr(0, 1));
    initials.append(middle.substr(0, 1));
    initials.append(last.substr(0, 1));

    cout << (gender == "M" ? "Mr." : "Mrs.") << " " << last << ", your initials are " << initials << endl;

    return 0;

}
