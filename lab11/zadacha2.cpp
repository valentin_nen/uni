#include <iostream>
#include <vector>
#include <cctype>

using namespace std;

int digit(char c)
{
    return c - '0';
}

int todec(const string &bin)
{
    int result = 0;

    for(int i=bin.length()-1, p=1; i >=0; i--, p*=2)
    {
        result += p* digit(bin[i]);
    }

    return result;
}

int main()
{
    string binary;
    cin >> binary;

    cout << todec(binary) << endl;

    return 0;
}
