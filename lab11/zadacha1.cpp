#include <iostream>
#include <vector>
#include <cctype>

using namespace std;

void count_all(int number, int &odd, int &even)
{
    odd = 0, even = 0;
    while(number)
    {
        int lastDigit = number % 10;
        if(lastDigit % 2)
            odd++;
        else
            even++;

        number /= 10;
    }
}

int count_odd(int number)
{
    int cnt = 0;
    while(number)
    {
        int lastDigit = number % 10;
        if(lastDigit % 2)
            cnt++;
        number /= 10;
    }

    return cnt;
}

int count_even(int number)
{
     int cnt = 0;
    while(number)
    {
        int lastDigit = number % 10;
        if(lastDigit % 2 == 0)
            cnt++;
        number /= 10;
    }

    return cnt;
}

int main()
{
    int n;
    cin >> n;
    for(int i=0; i<n; i++)
    {
        int number;
        cin >> number;
        //cout << count_odd(number) << " " << count_even(number) << endl;
        //vector<int> cnts = count_all(number);
        //cout << cnts[0] << " " << cnts[1] << endl;
        int odd, even;
        count_all(number, odd, even);
        cout << odd << " " << even << endl;
    }
    return 0;
}
